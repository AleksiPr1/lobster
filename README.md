# Lightcurve analysis tool

This is a developpement repository that permits to analyze a given lightcurve.

This work is based on (heavily or loosely, depending on the part of the code):
- [Lightkurve](https://github.com/lightkurve/lightkurve)
- [Platano](https://gitlab.com/rgarcibus/platano)
- the [KASOC filter](https://github.com/tasoc/corrections/blob/devel/corrections/kasoc_filter/kasoc_filter.py)

## How to use

Multiple examples on how to use this module are shown in the [notebooks/](https://gitlab.com/aunetx/lobster/-/blob/master/notebooks) subfolder, as a Jupyter notebook. More particularly, a step-by-step tutorial covering some importants parts is present at [notebooks/example_tess.ipynb](https://gitlab.com/aunetx/lobster/-/blob/master/notebooks/example_tess.ipynb).

For the moment, there is no module documentation; however most (if not every) function and method
should be documented as precisely as possible in the code with docstrings.

## Requirements

To run it, you will need:

- [numpy](https://numpy.org/)
- [matplotlib](https://matplotlib.org/)
- [astropy](https://www.astropy.org/)
- [apollinaire](https://gitlab.com/sybreton/apollinaire)

## License

This program is distributed under the terms of the GNU General Public License, version 3 or later.