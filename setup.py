from setuptools import setup, find_packages

setup(
    name="lobster",
    version="1.0.4",
    author="Aurélien Hamy",
    author_email="aurelien.hamy@student-cs.fr",
    description="Pipeline for light curve corrections",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aunetx/lobster",
    packages=find_packages(),
    install_requires=[
        "numpy",
        "astropy",
        "matplotlib",
        "apollinaire",
        "pandas",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Operating System :: OS Independent",
    ],
    python_requires=">3.10",
)
