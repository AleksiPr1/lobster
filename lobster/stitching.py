import matplotlib.pyplot as plt
import numpy as np
import copy

from lobster.utils import *


def stitch(times, fluxes, jumps, poly_min_valid_data_points=100, debug=False, verbose=True):
    used_jumps = copy.deepcopy(jumps)

    for i, jump in enumerate(jumps):
        flux_left, flux_right = fluxes[i], fluxes[i + 1]
        time_left, time_right = times[i], times[i + 1]
        t_jump = time_right[0]

        stitching_mode = jumps[jump]["stitching_mode"]
        max_width_days = jumps[jump]["max_width_days"]
        max_jump_poly_1 = jumps[jump]["max_jump_poly_1"]
        max_jump_poly_2 = jumps[jump]["max_jump_poly_2"]
        max_jump_multiplicator_poly_1 = jumps[jump]["max_jump_multiplicator_poly_1"]
        max_jump_multiplicator_poly_2 = jumps[jump]["max_jump_multiplicator_poly_2"]
        correction_type = jumps[jump]["correction_type"]

        j = i
        while j > 0 and is_all_invalid(flux_left):
            # if the left flux is invalid, then we search fluxes before to try stitching anyway
            if debug and verbose:
                print(
                    f"Jump at t={t_jump:.4f} d, left flux is entirely invalid, taking one before it"
                )
            j -= 1
            flux_left = fluxes[j]
            time_left = times[j]

        fig = None
        ax = None
        if debug:
            fig = plt.figure(figsize=(10, 6))
            ax = fig.add_subplot()
            ax.scatter(time_left, flux_left, label="flux left", s=1.4, c="royalblue")
            ax.scatter(time_right, flux_right, label="flux right", s=1.4, c="lightcoral")

        if is_all_invalid(flux_left):
            # no need to do anything: we have no baseline data on the left
            if debug and verbose:
                print(f"Cannot adjust for jump at t={t_jump:.4f} d, left flux is entirely invalid")
            diff = 0

        elif is_all_invalid(flux_right):
            # no need to do anything: the data we want to adjust on the right is invalid
            if debug and verbose:
                print(f"Cannot adjust for jump at t={t_jump:.4f} d, right flux is entirely invalid")
            diff = 0

        else:
            # left and right fluxes with only valid values
            valid_flux_left = flux_left[np.isfinite(flux_left)]
            valid_flux_right = flux_right[np.isfinite(flux_right)]

            # left and right times for the previous fluxes
            valid_time_left = time_left[np.isfinite(flux_left)]
            valid_time_right = time_right[np.isfinite(flux_right)]

            # the jump in time actually happening
            real_jump_gap = valid_time_right[0] - valid_time_left[-1]

            # * tips used to fit the polynomials

            # the begin (end) time for the left (right) section we would like to see hapenning
            poly_left_tip_begin_time = valid_time_left[-1] - max_width_days
            poly_right_tip_end_time = valid_time_right[0] + max_width_days

            # the actual bin corresponding to the closest begin (end) time for the left (right)
            # section, which encompasses the entire valid section if it is too small
            poly_left_tip_begin_bin = np.searchsorted(
                valid_time_left, poly_left_tip_begin_time, side="left"
            )
            poly_right_tip_end_bin = np.searchsorted(
                valid_time_right, poly_right_tip_end_time, side="right"
            )

            # the left and right tips fluxes
            poly_left_tip_flux = valid_flux_left[poly_left_tip_begin_bin:]
            poly_right_tip_flux = valid_flux_right[:poly_right_tip_end_bin]

            # the left and right tips times
            poly_left_tip_time = valid_time_left[poly_left_tip_begin_bin:]
            poly_right_tip_time = valid_time_right[:poly_right_tip_end_bin]

            # * tips used to calculate the BIC for the polynomial, overshoots the previous tips
            # * up to the encounter point

            # the begin (end) time for the left (right) section we will see hapenning
            poly_left_tip_BIC_begin_time = poly_left_tip_begin_time - real_jump_gap / 2
            poly_right_tip_BIC_end_time = poly_right_tip_end_time + real_jump_gap / 2

            # the bin corresponding to the previous times
            poly_left_tip_BIC_begin_bin = np.searchsorted(
                valid_time_left, poly_left_tip_BIC_begin_time, side="left"
            )
            poly_right_tip_BIC_end_bin = np.searchsorted(
                valid_time_right, poly_right_tip_BIC_end_time, side="right"
            )

            # the left and right tips fluxes for the BIC testing
            poly_left_tip_BIC_flux = valid_flux_left[poly_left_tip_BIC_begin_bin:]
            poly_right_tip_BIC_flux = valid_flux_right[:poly_right_tip_BIC_end_bin]

            # the left and right tips times for the BIC testing
            poly_left_tip_BIC_time = valid_time_left[poly_left_tip_BIC_begin_bin:]
            poly_right_tip_BIC_time = valid_time_right[:poly_right_tip_BIC_end_bin]

            # * tips used to calculate the mean value, which is the same as the polynomial tip but
            # * constrained to be of equal duration

            # the tip width if we constrain the left and right tip to be of equal duration
            tip_width_constrained_equal = min(
                poly_left_tip_time[-1] - poly_left_tip_time[0],
                poly_right_tip_time[-1] - poly_right_tip_time[0],
            )

            # the begin (end) time for the left (right) section we will see hapenning
            mean_left_tip_begin_time = valid_time_left[-1] - tip_width_constrained_equal
            mean_right_tip_end_time = valid_time_right[0] + tip_width_constrained_equal

            # the actual bin corresponding to the closest begin (end) time for the left (right)
            # section, which encompasses the entire valid section if it is too small
            mean_left_tip_begin_bin = np.searchsorted(
                valid_time_left, mean_left_tip_begin_time, side="left"
            )
            mean_right_tip_end_bin = np.searchsorted(
                valid_time_right, mean_right_tip_end_time, side="right"
            )

            # the left and right tips fluxes, constrained to be the same duration
            mean_left_tip_flux = valid_flux_left[mean_left_tip_begin_bin:]
            mean_right_tip_flux = valid_flux_right[:mean_right_tip_end_bin]

            # the left and right tips times, constrained to be the same duration
            mean_left_tip_time = valid_time_left[mean_left_tip_begin_bin:]
            mean_right_tip_time = valid_time_right[:mean_right_tip_end_bin]

            # * tips used to calculate the BIC for the mean value, overshoots the previous tips
            # * up to the encounter point

            # the begin (end) time for the left (right) section we will see hapenning
            mean_left_tip_BIC_begin_time = mean_left_tip_begin_time - real_jump_gap / 2
            mean_right_tip_BIC_end_time = mean_right_tip_end_time + real_jump_gap / 2

            # the bin corresponding to the previous times
            mean_left_tip_BIC_begin_bin = np.searchsorted(
                valid_time_left, mean_left_tip_BIC_begin_time, side="left"
            )
            mean_right_tip_BIC_end_bin = np.searchsorted(
                valid_time_right, mean_right_tip_BIC_end_time, side="right"
            )

            # the left and right tips fluxes for the BIC testing
            mean_left_tip_BIC_flux = valid_flux_left[mean_left_tip_BIC_begin_bin:]
            mean_right_tip_BIC_flux = valid_flux_right[:mean_right_tip_BIC_end_bin]

            # if using tips-mean mode, then adjust using mean for the ends of the sections
            if stitching_mode == "tips-mean":
                if debug and verbose:
                    print(f"Using tips-mean for jump at t={t_jump:.4f} d")

                diff = tips_mean(
                    mean_left_tip_flux,
                    mean_right_tip_flux,
                    mean_left_tip_time,
                    mean_right_tip_time,
                    mean_left_tip_BIC_flux,
                    mean_right_tip_BIC_flux,
                    debug,
                    ax,
                )[0]

            # if using total-mean mode, then adjust using mean for the entirety of the sections
            elif stitching_mode == "total-mean":
                if debug and verbose:
                    print(f"Using total-mean for jump at t={t_jump:.4f} d")

                diff = total_mean(
                    valid_flux_left, valid_flux_right, valid_time_left, valid_time_right, debug, ax
                )[0]

            # if using tips-poly-1 mode, then adjust using polynomial of order 1 fitting
            elif stitching_mode == "tips-poly-1":
                if debug and verbose:
                    print(f"Using tips-poly-1 for jump at t={t_jump:.4f} d")

                if real_jump_gap > 2 * max_width_days:
                    print(
                        f"Using tips-poly for a jump that is {real_jump_gap / max_width_days:.2f} times the tip width!"
                    )

                diff = tips_poly(
                    1,
                    poly_left_tip_flux,
                    poly_right_tip_flux,
                    poly_left_tip_time,
                    poly_right_tip_time,
                    poly_left_tip_BIC_flux,
                    poly_right_tip_BIC_flux,
                    poly_left_tip_BIC_time,
                    poly_right_tip_BIC_time,
                    debug,
                    ax,
                )[0]

            # if using tips-poly-2 mode, then adjust using polynomial of order 2 fitting
            elif stitching_mode == "tips-poly-2":
                if debug and verbose:
                    print(f"Using tips-poly-2 for jump at t={t_jump:.4f} d")

                if real_jump_gap > 2 * max_width_days:
                    print(
                        f"Using tips-poly for a jump that is {real_jump_gap / max_width_days:.2f} times the tip width!"
                    )

                diff = tips_poly(
                    2,
                    poly_left_tip_flux,
                    poly_right_tip_flux,
                    poly_left_tip_time,
                    poly_right_tip_time,
                    poly_left_tip_BIC_flux,
                    poly_right_tip_BIC_flux,
                    poly_left_tip_BIC_time,
                    poly_right_tip_BIC_time,
                    debug,
                    ax,
                )[0]

            # if using BIC mode, then choose the best method using BIC
            elif stitching_mode == "BIC":
                diff_tips_mean, estimator_tips_mean = tips_mean(
                    mean_left_tip_flux,
                    mean_right_tip_flux,
                    mean_left_tip_time,
                    mean_right_tip_time,
                    mean_left_tip_BIC_flux,
                    mean_right_tip_BIC_flux,
                    debug,
                    ax,
                    estimator=lambda data, model: BIC(data, model, 2),
                )

                diff_total_mean, estimator_total_mean = total_mean(
                    valid_flux_left,
                    valid_flux_right,
                    valid_time_left,
                    valid_time_right,
                    debug,
                    ax,
                    estimator=lambda data, model: BIC(data, model, 2),
                )

                diff_tips_poly_1, estimator_tips_poly_1 = tips_poly(
                    1,
                    poly_left_tip_flux,
                    poly_right_tip_flux,
                    poly_left_tip_time,
                    poly_right_tip_time,
                    poly_left_tip_BIC_flux,
                    poly_right_tip_BIC_flux,
                    poly_left_tip_BIC_time,
                    poly_right_tip_BIC_time,
                    debug,
                    ax,
                    estimator=lambda data, model: BIC(data, model, 4),
                )

                diff_tips_poly_2, estimator_tips_poly_2 = tips_poly(
                    2,
                    poly_left_tip_flux,
                    poly_right_tip_flux,
                    poly_left_tip_time,
                    poly_right_tip_time,
                    poly_left_tip_BIC_flux,
                    poly_right_tip_BIC_flux,
                    poly_left_tip_BIC_time,
                    poly_right_tip_BIC_time,
                    debug,
                    ax,
                    estimator=lambda data, model: BIC(data, model, 6),
                )

                # do not use polynomial fitting if the gap is too big relative to the fitted data,
                # or in absolute days
                poly_actual_tip_width = min(
                    poly_left_tip_time[-1] - poly_left_tip_time[0],
                    poly_right_tip_time[-1] - poly_right_tip_time[0],
                )
                if real_jump_gap > min(
                    max_jump_multiplicator_poly_1 * poly_actual_tip_width, max_jump_poly_1
                ):
                    estimator_tips_poly_1 = np.infty
                if real_jump_gap > min(
                    max_jump_multiplicator_poly_2 * poly_actual_tip_width, max_jump_poly_2
                ):
                    estimator_tips_poly_2 = np.infty

                i = np.argmin(
                    [
                        estimator_tips_mean,
                        estimator_total_mean,
                        estimator_tips_poly_1,
                        estimator_tips_poly_2,
                    ]
                )
                diff = [diff_tips_mean, diff_total_mean, diff_tips_poly_1, diff_tips_poly_2][i]
                chosen_stitching_mode = [
                    "BIC-tips-mean",
                    "BIC-total-mean",
                    "BIC-tips-poly-1",
                    "BIC-tips-poly-2",
                ][i]

                used_jumps[jump]["stitching_mode"] = chosen_stitching_mode

                if debug:
                    fig.suptitle(f"Choosen {chosen_stitching_mode}")
                    if verbose:
                        print(f"BIC: choosing {chosen_stitching_mode} for jump at t={t_jump:.4f} d")

            else:
                raise ValueError(
                    f"could not find jump stitching mode, unknown mode '{stitching_mode}'"
                )

        if correction_type == "additive":
            for flux in fluxes[j + 1 :]:
                flux -= diff
        elif correction_type == "multiplicative":
            first_val = fluxes[j + 1][np.isfinite(fluxes[j + 1])][0]
            for flux in fluxes[j + 1 :]:
                flux *= (first_val - diff) / first_val
        else:
            raise ValueError(
                f"could not find jump correction type, unknown type '{correction_type}'"
            )

        if debug:
            ax.scatter(
                time_right,
                fluxes[j + 1],
                label="stitched",
                s=1,
                c="blueviolet",
                zorder=-100,
            )
            ax.legend(fontsize="small", ncols=2)
            ax.set_xlabel("Time (d)")
            ax.set_ylabel("Flux")
            fig.tight_layout()
            fig.show()

    return np.concatenate(fluxes), used_jumps


def BIC(data, model, dof, scatter=None):
    """
    Calculate the Bayesian Information Criterion given data, model and degrees of freedom.
    Parameters:
            data (ndarray):
            model (ndarray):
            dof (int): Number of degrees of freedom in the model.
            scatter (ndarray, optional):
                    If not provided, a robust mean scatter is calculated from the data.
    Returns:
            float: Bayesian Information Criterion.
    """
    if scatter is None:
        # : Conversion constant from MAD to Sigma. Constant is 1/norm.ppf(3/4)
        mad_to_sigma = 1.482602218505602
        scatter = np.nanmedian(np.abs(np.diff(data))) * mad_to_sigma
    return (
        len(data) * np.log(2)
        + np.nansum((data - model) ** 2 / scatter**2)
        + dof * np.log(len(data))
    )


def tips_mean(
    tip_left_flux,
    tip_right_flux,
    tip_left_time,
    tip_right_time,
    estimator_flux_left,
    estimator_flux_right,
    debug,
    ax,
    estimator=lambda x, y: None,
):
    mean_left = np.nanmean(tip_left_flux)
    mean_right = np.nanmean(tip_right_flux)

    if debug:
        ax.scatter(tip_left_time, tip_left_flux, label="tip mean left", s=1.7, c="darkblue")
        ax.scatter(tip_right_time, tip_right_flux, label="tip mean right", s=1.7, c="maroon")
        ax.axhline(mean_left, label="tip mean left", ls=":", lw=0.9, c="darkblue")
        ax.axhline(mean_right, label="tip mean right", ls=":", lw=0.9, c="maroon")

    data = np.concatenate((estimator_flux_left, estimator_flux_right))
    model = np.concatenate(
        (
            np.full_like(estimator_flux_left, mean_left),
            np.full_like(estimator_flux_right, mean_right),
        )
    )
    return mean_right - mean_left, estimator(data, model)


def total_mean(
    flux_left,
    flux_right,
    time_left,
    time_right,
    debug,
    ax,
    estimator=lambda x, y: None,
):
    mean_left = np.nanmean(flux_left)
    mean_right = np.nanmean(flux_right)

    if debug:
        ax.axhline(mean_left, label="total mean left", ls=":", c="royalblue", lw=0.9)
        ax.axhline(mean_right, label="total mean right", ls=":", c="salmon", lw=0.9)

    data = np.concatenate((flux_left, flux_right))
    model = np.concatenate(
        (np.full_like(time_left, mean_left), np.full_like(time_right, mean_right))
    )
    return mean_right - mean_left, estimator(data, model)


def tips_poly(
    poly_order,
    left_flux,
    right_flux,
    left_time,
    right_time,
    left_BIC_flux,
    right_BIC_flux,
    left_BIC_time,
    right_BIC_time,
    debug,
    ax,
    estimator=lambda x, y: None,
):
    if sum(np.isfinite(left_flux)) <= poly_order or sum(np.isfinite(right_flux)) <= poly_order:
        if estimator:
            return 0, np.infty
        else:
            print(f"Using polynomial fitting when there is only one value at t={left_time[-1]}")
            return 0

    poly_left = np.polynomial.Polynomial.fit(
        left_time[np.isfinite(left_flux)],
        left_flux[np.isfinite(left_flux)],
        poly_order,
    )
    poly_right = np.polynomial.Polynomial.fit(
        right_time[np.isfinite(right_flux)],
        right_flux[np.isfinite(right_flux)],
        poly_order,
    )
    middle_time = (left_time[-1] + right_time[0]) / 2

    if debug:
        ax.scatter(
            left_time,
            left_flux,
            label=f"tip poly {poly_order} left",
            s=1.5,
            c="mediumblue",
        )
        ax.scatter(
            right_time,
            right_flux,
            label=f"tip poly {poly_order} right",
            s=1.5,
            c="firebrick",
        )
        poly_time_left = np.linspace(left_BIC_time[0], middle_time, 500)
        poly_time_right = np.linspace(middle_time, right_BIC_time[-1], 500)
        ax.plot(
            poly_time_left,
            poly_left(poly_time_left),
            label=f"poly {poly_order} left",
            ls="--",
            c=["darkorange", "gold"][poly_order - 1],
            lw=1.5,
        )
        ax.plot(
            poly_time_right,
            poly_right(poly_time_right),
            label=f"poly {poly_order} right",
            ls="--",
            c=["limegreen", "aquamarine"][poly_order - 1],
            lw=1.5,
        )
        ax.axvline(
            middle_time, label="polynomial interception", ls="--", c="black", lw=0.7, alpha=0.5
        )

    data = np.concatenate((left_BIC_flux, right_BIC_flux))
    model = np.concatenate((poly_left(left_BIC_time), poly_right(right_BIC_time)))
    return poly_right(middle_time) - poly_left(middle_time), estimator(data, model)
