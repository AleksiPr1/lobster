import numpy as np
import matplotlib.pyplot as plt
from astropy.convolution import convolve, convolve_fft
from lobster.utils import *


def sin4_filter(data, window_size, *args, **kwargs):
    """
    Applies a sin4 smoothing filter to the input data.
    Input:
    - data, a np.array representing a flux
    - window_size, an odd integer, the size of the filter window
    - padding_modes, a tuple of two strings -- for left and right, each being one of:
        - "mirror", to mirror the data along the vertical axis
        - "mirror-both", to mirror the data along the vertical and horizontal axis
        - "zero", to pad the data with zeros
    - convolve_mode, either 'direct' or 'fft', to choose the astropy convolution mode
    Output:
    - the filtered data, a np.array of the same size as its input
    """
    if window_size > 1:
        lin = np.linspace(0, np.pi, window_size)
        window = np.sin(lin) ** 4
    else:
        window = np.ones(window_size)
    return filter_with_window(data, window_size, window, *args, **kwargs)


def triangular_filter(data, window_size, *args, **kwargs):
    """
    Applies a triangular smoothing filter to the input data.
    Input:
    - data, a np.array representing a flux
    - window_size, an odd integer, the size of the filter window
    - padding_modes, a tuple of two strings -- for left and right, each being one of:
        - "mirror", to mirror the data along the vertical axis
        - "mirror-both", to mirror the data along the vertical and horizontal axis
        - "zero", to pad the data with zeros
    - convolve_mode, either 'direct' or 'fft', to choose the astropy convolution mode
    Output:
    - the filtered data, a np.array of the same size as its input
    """
    window = np.concatenate(
        (
            np.arange(1, window_size // 2 + 1),
            [window_size // 2 + 1],
            np.arange(window_size // 2, 0, -1),
        )
    )
    return filter_with_window(data, window_size, window, *args, **kwargs)


def boxcar_filter(data, window_size, *args, **kwargs):
    """
    Applies a boxcar smoothing filter to the input data.
    Input:
    - data, a np.array representing a flux
    - window_size, an odd integer, the size of the filter window
    - padding_modes, a tuple of two strings -- for left and right, each being one of:
        - "mirror", to mirror the data along the vertical axis
        - "mirror-both", to mirror the data along the vertical and horizontal axis
        - "zero", to pad the data with zeros
    - convolve_mode, either 'direct' or 'fft', to choose the astropy convolution mode
    Output:
    - the filtered data, a np.array of the same size as its input
    """
    window = np.ones(window_size)
    return filter_with_window(data, window_size, window, *args, **kwargs)


def pad_mirror_both(data, n_points_to_pad, n_points_median=30):
    n_points_orig = len(data)

    if n_points_median <= len(data):
        median_left = np.nanmedian(data[:n_points_median])
        median_right = np.nanmedian(data[-n_points_median:])
    else:
        median_left = np.nanmedian(data)
        median_right = np.nanmedian(data)

    n_points_padded_left = min(n_points_to_pad[0], len(data))
    n_points_padded_right = min(n_points_to_pad[1], len(data))

    if n_points_padded_left > 0:
        data_left = 2 * median_left - data[::-1][-n_points_to_pad[0] :]
        data = np.concatenate((data_left, data))
    if n_points_padded_right > 0:
        data_right = 2 * median_right - data[::-1][: n_points_to_pad[1]]
        data = np.concatenate((data, data_right))

    if len(data) < n_points_orig + sum(n_points_to_pad):
        return pad_mirror_both(
            data,
            (n_points_to_pad[0] - n_points_padded_left, n_points_to_pad[1] - n_points_padded_right),
            n_points_median,
        )
    else:
        return data


def filter_with_window(
    data,
    window_size,
    window,
    padding_modes=("mirror_both", "mirror_both"),
    convolve_mode="fft",
    preserve_nan=True,
):
    """
    Applies a filter with a given window to the input data.
    Input:
    - data, a np.array representing a flux
    - window_size, an odd integer, the size of the filter window
    - window, a np.array of size window_size, to be convolved with the data
    - padding_modes, a tuple of two strings -- for left and right, each being one of:
        - "mirror", to mirror the data along the vertical axis
        - "mirror-both", to mirror the data along the vertical and horizontal axis
        - "zero", to pad the data with zeros
    - convolve_mode, either 'direct' or 'fft', to choose the astropy convolution mode
    Output:
    - the filtered data, a np.array of the same size as its input
    """
    assert window_size % 2 == 1
    if is_all_invalid(data):
        return data

    # first an last nonnan values
    first_nonnan = np.nonzero(np.isfinite(data))[0][0]
    last_nonnan = np.nonzero(np.isfinite(data))[0][-1]

    # data that is valid at least on its tips (to do the padding...)
    valid_data = data[first_nonnan : last_nonnan + 1]

    padded_data = np.copy(valid_data)
    for padding_mode, pad in zip(padding_modes, [(window_size // 2, 0), (0, window_size // 2)]):
        if padding_mode == "mirror":
            padded_data = np.pad(padded_data, pad, "reflect")
        elif padding_mode == "mirror_both":
            padded_data = pad_mirror_both(padded_data, pad)
        elif padding_mode == "zero":
            padded_data = np.pad(padded_data, pad, "constant", constant_values=(0, 0))
        else:
            raise ValueError(f"incorrect padding mode: '{padding_mode}'")

    if convolve_mode == "direct":
        convolve_function = convolve
    elif convolve_mode == "fft":
        convolve_function = convolve_fft
    else:
        raise ValueError(f"could not find convolution mode '{convolve_mode}'")

    convolved = convolve_function(padded_data, window / window.sum(), preserve_nan=preserve_nan)

    if window_size == 1:
        return convolved
    else:
        return np.concatenate(
            (
                np.full(first_nonnan, np.nan),
                convolved[window_size // 2 : -window_size // 2 + 1],
                np.full(len(data) - last_nonnan - 1, np.nan),
            )
        )


def adaptive_filter(
    data,
    window_sizes,
    thresholds,
    filter_function=sin4_filter,
    padding_modes=("mirror", "mirror"),
    convolve_mode="fft",
    debug=False,
    time=None,
):
    """
    Applies an adaptative filter which automatically switches from a little window size to a big
    one. It calculate a little and a big filter to the data, its difference, and when this
    difference falls outside its standard deviation (which mean that the big filter cannot follow
    the data adequately), then the little filter is progressively used.
    Input:
    - data, a np.array representing a flux
    - window_sizes, a tuple of two odd integers for the little and big filter windows
    - thresholds, a tuple of two floats, each one for:
        - the minimum standard deviation of the difference to its average in order to begin the use
          of the little filter
        - the maximum standard deviation of the difference to its average in order to stop using the
          big filter (between the two threshold values, a weighted average of the two filters is
          used)
    - padding_modes, a tuple of two strings -- for left and right, each being one of:
        - "mirror", to mirror the data along the vertical axis
        - "mirror-both", to mirror the data along the vertical and horizontal axis
        - "zero", to pad the data with zeros
    - convolve_mode, either 'direct' or 'fft', to choose the astropy convolution mode
    - debug, a boolean, True to plot the adaptative filter informations
    - time, a np.array representing the timestamps of the data, useful for visualising with debug
    Output:
    - the filtered data, a np.array of the same size of its input
    - the outliers indicator, a np.array of the same size as its input, containing informations
      about when the big filter was used (close to 0) and when the little filter was used (strictly
      positive and close to 1)
    """
    little_filter = filter_function(data, window_sizes[0], padding_modes, convolve_mode)
    big_filter = filter_function(data, window_sizes[1], padding_modes, convolve_mode)
    diff = little_filter - big_filter

    mean = np.nanmean(diff)
    sigma = np.nanstd(diff)
    centered_diff = abs(diff - mean) / sigma
    indicator_outlier = (
        np.minimum(thresholds[1], np.maximum(thresholds[0], centered_diff)) - thresholds[0]
    ) / (thresholds[1] - thresholds[0])

    filtered = little_filter * indicator_outlier + big_filter * (1 - indicator_outlier)

    if debug:
        percentage_little_filter = sum(indicator_outlier > 0) / len(indicator_outlier) * 100
        if time is None:
            time = np.arange(len(filtered))

        fig = plt.figure(figsize=(10, 6))
        ax = fig.add_subplot()
        ax.set_title(
            f"Debug graph for adaptative filter, {percentage_little_filter:.2f}% little filter used"
        )
        ax.set_xlabel(r"Time (d)")
        ax.set_ylabel(r"Flux")
        ax.plot(time, data, label="original flux")
        ax.plot(time, little_filter, alpha=0.4, label="little filter")
        ax.plot(time, big_filter, alpha=0.4, label="big filter")
        ax.scatter(
            time,
            filtered,
            s=0.6,
            alpha=0.6,
            c=indicator_outlier,
            cmap="viridis",
            label="adaptative filter",
            zorder=3,
        )
        ax.fill_between(
            time,
            0,
            1,
            where=indicator_outlier > 0.0,
            alpha=0.3,
            label="little filter used",
            transform=ax.get_xaxis_transform(),
        )
        ax.legend()
        ax.grid()

    return filtered, indicator_outlier
