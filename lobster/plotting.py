import matplotlib.pyplot as plt


def plot(
    x,
    y,
    figsize=(10, 4),
    dpi=100,
    ax=None,
    xlabel=None,
    ylabel=None,
    xscale="linear",
    yscale="linear",
    xlim=None,
    ylim=None,
    title=None,
    grid=True,
    duplicate_axes=True,
    show=False,
    path_save=None,
    alpha=0.7,
    label=None,
    **kwargs,
):
    """
    A general function to plot anything with the correct visual at first glance.
    Input:
    - x and y, the data to be plotted
    - ax, a matplotlib axis to use for the plotting rather than creating a new one
    - xlabel and ylabel, the labels for the x and y axis
    - xscale and yscale, the scales for the x and y axis (by default linear)
    - xlim and ylim, the limits for the x and y axis (by default using the default ones)
    - title, the general title of the figure
    - show, whether or not to explicitely show the figure
    - path_save, if not None, will save the figure to the given path
    Any additional argument will be passed to matplotlib.
    Returns the associated axis.
    """
    if ax is None:
        fig = plt.figure(figsize=figsize, dpi=dpi)
        ax = fig.add_subplot()
    else:
        fig = ax.get_figure()

    ax.plot(x, y, label=label, alpha=alpha, **kwargs)

    # ensure we don't perturb previous plots if the values are unset
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    if xscale:
        ax.set_xscale(xscale)
    if yscale:
        ax.set_yscale(yscale)
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    if title:
        ax.set_title(title)
    if grid and not (hasattr(ax, "grid_is_enabled") and ax.grid_is_enabled):
        # needed or the grid is disabled when plotting two times
        ax.grid_is_enabled = True
        ax.grid()
    if duplicate_axes:
        ax.tick_params(axis="x", which="both", bottom=True, top=True)
        ax.tick_params(axis="y", which="both", left=True, right=True)
    if label:
        ax.legend()

    if path_save:
        fig.savefig(path_save)
    if show:
        fig.tight_layout()
        plt.show()
    return ax


def scatter(
    x,
    y,
    figsize=(10, 4),
    dpi=100,
    ax=None,
    xlabel=None,
    ylabel=None,
    xscale="linear",
    yscale="linear",
    xlim=None,
    ylim=None,
    title=None,
    grid=True,
    duplicate_axes=True,
    show=False,
    path_save=None,
    alpha=0.5,
    s=4,
    label=None,
    **kwargs,
):
    """
    A general function to plot anything with the correct visual at first glance.
    Input:
    - x and y, the data to be plotted
    - ax, a matplotlib axis to use for the plotting rather than creating a new one
    - xlabel and ylabel, the labels for the x and y axis
    - xscale and yscale, the scales for the x and y axis (by default linear)
    - xlim and ylim, the limits for the x and y axis (by default using the default ones)
    - title, the general title of the figure
    - show, whether or not to explicitely show the figure
    - path_save, if not None, will save the figure to the given path
    Any additional argument will be passed to matplotlib.
    Returns the associated axis.
    """
    if ax is None:
        fig = plt.figure(figsize=figsize, dpi=dpi)
        ax = fig.add_subplot()
    else:
        fig = ax.get_figure()

    ax.scatter(x, y, label=label, alpha=alpha, s=s, **kwargs)

    # ensure we don't perturb previous plots if the values are unset
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    if xscale:
        ax.set_xscale(xscale)
    if yscale:
        ax.set_yscale(yscale)
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    if title:
        ax.set_title(title)
    if grid and not (hasattr(ax, "grid_is_enabled") and ax.grid_is_enabled):
        # needed or the grid is disabled when plotting two times
        ax.grid_is_enabled = True
        ax.grid()
    if duplicate_axes:
        ax.tick_params(axis="x", which="both", bottom=True, top=True)
        ax.tick_params(axis="y", which="both", left=True, right=True)
    if label:
        ax.legend()

    if path_save:
        fig.savefig(path_save)
    if show:
        fig.tight_layout()
        plt.show()
    return ax
