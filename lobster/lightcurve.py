import copy, os
import numpy as np
from astropy.io import fits
from astropy.table import Table, vstack
import apollinaire as apn
import matplotlib.transforms as transforms

from lobster.stitching import *
from lobster.filtering import *
from lobster.utils import *
from lobster.plotting import *

# see notebooks/cutting_periods.ipynb for the justification
# link between window size and cutting period for a 0.9 gain (1.0 gain does not exist)
SMOOTH_BOXCAR_FILTER_DIVISER = 4
SMOOTH_TRIANGULAR_FILTER_DIVISER = 3
SMOOTH_SIN4_FILTER_DIVISER = 2
# link between window size and cutting period for a 1.0 gain
NORMALIZE_BOXCAR_FILTER_DIVISER = 1.00
NORMALIZE_TRIANGULAR_FILTER_DIVISER = 0.532
NORMALIZE_SIN4_FILTER_DIVISER = 0.354
NORMALIZE_SIN4_3_TIMES_FILTER_DIVISER = 0.340


class Lightcurve:
    # object name identifier
    object_name = None
    # used in the plot
    unit = "unit"
    # used as default plotting options
    figsize = (10, 6)
    dpi = 100
    # if True, warnings will be shown
    verbose = True
    # if True, the time and flux will be saved in self.operations
    _debug = False

    # * -------------------- LIGHTCURVE CREATION -------------------- * #

    def __init__(self, path=None):
        """
        Initializes the lightcurve object. If a path is given, the time and flux is loaded from the
        associated FITS file.
        """
        self.operations = []
        self.object_name = ""
        if path is not None:
            if "kbonus" in path:
                self.load_kbonus(path)
            elif "qlp" in path:
                self.load_qlp(path)
            else:
                print(f"could not find the correct lightcurve type for path {path}")

    def copy(self):
        """
        Copy the lightcurve object, making a deepcopy of its arrays.
        """
        return copy.deepcopy(self)

    def write_to_fits(self, filename):
        """
        Writes the lightcurve object as a FITS file.
        """
        write_to_fits(self.time, self.flux, self.operations, self.unit, filename, self.object_name)
        return self

    def init_from_data(self, time, flux, quality=None, sector=None, jumps={}):
        """
        Initializes a lightcurve given a time array, a flux array (of the same size) and optional
        quality array and jumps dictionnary.
        """
        N = len(time)
        self.time = np.copy(time)
        self.flux = np.copy(flux)
        self.jumps = copy.deepcopy(jumps)

        self.sort_by_time()

        # the original time, it NEVER changes
        self.original_time = np.copy(self.time)
        # same lenght as original time, but with the corresponding time in new time array
        self.corresponding_time = np.copy(self.time)
        # same lenght as original time
        self.original_quality = (
            np.full(N, -1, dtype=int) if quality is None else np.copy(quality).astype(int)
        )
        self.original_sector = (
            np.full(N, -1, dtype=int) if sector is None else np.copy(sector).astype(int)
        )

        return self

    def load_kbonus(
        self,
        fits_path,
        time="TIME",
        flux="FLUX",
        quality="SAP_QUALITY",
        sector="QUARTER",
    ):
        """
        Initializes a lightcurve from a single FITS file, reading every even HDU table beginning at
        the third one.
        It is the format KBONUS lightcurves use.
        """
        lightcurves_data = []
        with fits.open(fits_path, mode="readonly") as hdulist:
            self.object_name = hdulist[0].header["OBJECT"]
            for i in range(2, len(hdulist), 2):
                hdu = hdulist[i]
                assert isinstance(hdu, fits.hdu.table.BinTableHDU)
                data = np.copy(hdu.data)
                sector_num = hdu.header[sector]
                data = np.lib.recfunctions.append_fields(
                    data, sector, np.full(len(data[time]), sector_num, dtype=int)
                )
                lightcurves_data.append(Table(data))
                self.unit = hdu.columns[flux].unit

        self.add_operation(f"loaded as KBONUS from FITS, {len(lightcurves_data)} quarters read")
        return self.generate_from_quarters(
            lightcurves_data, time=time, flux=flux, quality=quality, sector=sector
        )

    def load_qlp(self, base_path, time="TIME", flux="SAP_FLUX", quality="QUALITY", sector="SECTOR"):
        """
        Initializes a lightcurve from a folder containing multiple FITS file, one for each quarter.
        It is the format QLP lightcurves use.
        """
        lightcurves_data = []
        for dirpath, dirnames, filenames in os.walk(base_path):
            for filename in filenames:
                fits_path = base_path + filename
                with fits.open(fits_path, mode="readonly") as hdulist:
                    self.object_name = hdulist[0].header["OBJECT"]
                    assert isinstance(hdulist[1], fits.hdu.table.BinTableHDU)
                    data = np.copy(hdulist[1].data)
                    sector_num = hdulist[0].header[sector]
                    data = np.lib.recfunctions.append_fields(
                        data, sector, np.full(len(data[time]), sector_num, dtype=int)
                    )
                    lightcurves_data.append(Table(data))

        assert len(lightcurves_data) > 0, "the FITS folder is empty"
        lightcurves_data = sorted(lightcurves_data, key=lambda x: x[time][0])

        self.add_operation(f"loaded as QLP from FITS, {len(lightcurves_data)} quarters read")
        return self.generate_from_quarters(
            lightcurves_data, time=time, flux=flux, quality=quality, sector=sector
        )

    def generate_from_quarters(
        self,
        lightcurves_data,
        initialize_jumps=True,
        time="TIME",
        flux="FLUX",
        quality="QUALITY",
        sector="SECTOR",
    ):
        """
        Given:
        - a list of FITS data for different lightcurves
        - whether or not to initialize the jumps
        - the time, flux and quality column names
        Initializes:
        - the jumps dictionnary
        - the time array
        - the flux array
        """
        data = vstack(lightcurves_data)
        self.time = np.array(data[time])
        self.flux = np.array(data[flux])

        jumps = {}
        for i in range(1, len(lightcurves_data)):
            jumps[sum(map(len, lightcurves_data[:i]))] = {}
        if initialize_jumps:
            self.jumps = dict(sorted(jumps.items()))

        self.sort_by_time()

        # the original time, it NEVER changes
        self.original_time = np.copy(self.time)
        # same lenght as original time, but with the corresponding time in new time array
        self.corresponding_time = np.copy(self.time)

        # same lenght as original time
        self.original_quality = np.array(data[quality])
        self.original_sector = np.array(data[sector])

        self.add_operation(f"generated from quarters, {len(self.jumps)} jumps added")
        return self

    def sort_by_time(self):
        # called at the very beginning, just after initializing time and flux!!

        if not np.all(self.time[:-1] < self.time[1:]):
            sorted_indices = np.argsort(self.time)

            self.time = self.time[sorted_indices]
            self.flux = self.flux[sorted_indices]

            jumps = {}
            new_indices = np.empty_like(sorted_indices)
            new_indices[sorted_indices] = np.arange(len(self.time))
            for jump_bin in self.jumps:
                new_jump_bin = new_indices[jump_bin]
                jumps[new_jump_bin] = self.jumps[jump_bin]
            self.jumps = dict(sorted(jumps.items()))

        assert np.all(self.time[:-1] < self.time[1:])
        return self

    @property
    def quality(self):
        # TODO do as Rafa told, with quality things
        time_to_original_indexes = np.searchsorted(self.corresponding_time, self.time)
        return np.where(
            np.logical_and(
                time_to_original_indexes >= 0,
                time_to_original_indexes < len(self.original_quality),
            ),
            self.original_quality[
                np.clip(time_to_original_indexes, 0, len(self.original_quality) - 1)
            ],
            -1,
        )

    @property
    def sector(self):
        time_to_original_indexes = np.searchsorted(self.corresponding_time, self.time)
        return np.where(
            np.logical_and(
                time_to_original_indexes >= 0,
                time_to_original_indexes < len(self.original_sector),
            ),
            self.original_sector[
                np.clip(time_to_original_indexes, 0, len(self.original_sector) - 1)
            ],
            -1,
        )

    # * -------------------- LIGHTCURVE PROPERTIES -------------------- * #

    @property
    def dt_median(self):
        """Returns the median of dt of the lightcurve."""
        return np.median(np.diff(self.time))

    @property
    def dt_mean(self):
        """Returns the mean of dt of the lightcurve."""
        return np.mean(np.diff(self.time))

    def duration_to_odd_size(self, duration, dt="median"):
        """
        Converts a duration (in days) to its related array size, using either the median of dt, its
        mean or a provided dt.
        The returned size will always be odd, as it is meant to be used for filter windows.
        """
        if dt == "median":
            dt = self.dt_median
        elif dt == "mean":
            dt = self.dt_mean
        return int(duration / dt) // 2 * 2 + 1

    def divide_flux_between_jumps(
        self,
        default_padding=("mirror_both", "mirror_both"),
        stitching_mode="BIC",
        correction_type="additive",
        max_width_days=3,
        max_jump_poly_1=30,
        max_jump_poly_2=10,
        max_jump_multiplicator_poly_1=10,
        max_jump_multiplicator_poly_2=3,
    ):
        """
        Cuts the lightcurve in-between jumps, and returns:
        - a list of time arrays for each section
        - a list of flux arrays for each section
        - a list of padding modes (left_mode, right_mode) corresponding to the left and right jumps
          next to each section
        """
        if len(self.jumps) > 0:
            padding_modes_list = (
                [[default_padding[0], None]]
                + [[None, None]] * (len(self.jumps) - 1)
                + [[None, default_padding[1]]]
            )
        else:
            padding_modes_list = [default_padding]

        fluxes = []
        times = []
        jumps = copy.deepcopy(self.jumps)
        start_index = 0
        for i, jump_index in enumerate(jumps):
            times.append(self.time[start_index:jump_index])
            fluxes.append(self.flux[start_index:jump_index])
            start_index = jump_index

            jump = jumps[jump_index]

            # set default values
            jump["padding_modes"] = jump.get("padding_modes", list(reversed(default_padding)))
            jump["stitching_mode"] = jump.get("stitching_mode", stitching_mode)
            jump["correction_type"] = jump.get("correction_type", correction_type)
            jump["max_width_days"] = jump.get("max_width_days", max_width_days)
            jump["max_jump_poly_1"] = jump.get("max_jump_poly_1", max_jump_poly_1)
            jump["max_jump_poly_2"] = jump.get("max_jump_poly_2", max_jump_poly_2)
            jump["max_jump_multiplicator_poly_1"] = jump.get(
                "max_jump_multiplicator_poly_1", max_jump_multiplicator_poly_1
            )
            jump["max_jump_multiplicator_poly_2"] = jump.get(
                "max_jump_multiplicator_poly_2", max_jump_multiplicator_poly_2
            )

            padding_modes_list[i][1] = jump["padding_modes"][0]
            padding_modes_list[i + 1][0] = jump["padding_modes"][1]

        times.append(self.time[start_index:])
        fluxes.append(self.flux[start_index:])
        return times, fluxes, jumps, padding_modes_list

    # * -------------------- STITCHING -------------------- * #

    def stitch(
        self,
        stitching_mode="BIC",  # can be BIC, tips-poly, tips-mean, or total-mean
        correction_type="additive",
        max_width_days=3,
        max_jump_poly_1=30,
        max_jump_poly_2=10,
        max_jump_multiplicator_poly_1=10,
        max_jump_multiplicator_poly_2=3,
    ):
        """
        Stitches the fluxes together at the jumps of the lightcurve.
        For each jump (and its associated left and right sections), it will try various methods:
        - if the right section or the whole left signal is all invalid, do nothing as nothing can be
          done anyway,
        - if only the left section is all invalid, equalize the mean between the end of the last
          valid portion of the flux on the left, and the first valid portion on the right,
        - if the gap in time of the jump is too big, use the same method,
        - else try a polynomial fitting method:
          - if the available and selected width of the data around the jump only contain invalids,
            use the same mean-equalization method as previously,
          - else, fit on the left and on the right of the jump, with a the given width limitation,
            a second order polynomial,
          - then calculate the difference between the left and the right polynomial, calculated in
            the middle of the jump, and do so this difference is zeroed.
        With this method, there is only a negligible probability that the stitching could go wrong
        for any given flux. However, probabilistic analysis of the stitched flux has not been
        implemented, and could help to choose whether or not to use the polynomial fitting at last.
        """
        times, fluxes, jumps, _ = self.divide_flux_between_jumps(
            stitching_mode=stitching_mode,
            correction_type=correction_type,
            max_width_days=max_width_days,
            max_jump_poly_1=max_jump_poly_1,
            max_jump_poly_2=max_jump_poly_2,
            max_jump_multiplicator_poly_1=max_jump_multiplicator_poly_1,
            max_jump_multiplicator_poly_2=max_jump_multiplicator_poly_2,
        )

        self.flux, used_jumps = stitch(times, fluxes, jumps, debug=self.debug, verbose=self.verbose)
        self.jumps = used_jumps

        self.add_operation(f"stitched fluxes")
        return self

    def equalize_mean_between_section_blocks(
        self,
        correction_type="additive",
        deviation_mode="std",
        detection_threshold=3,
        correction_threshold=6,
    ):
        """
        This looks at the mean of different blocks of sections, that are not far away.
        """
        times, fluxes, jumps, _ = self.divide_flux_between_jumps(
            stitching_mode="total-mean",
            correction_type=correction_type,
        )

        deviation_func = get_deviation(deviation_mode)
        original_jumps = self.jumps
        self.jumps = {}
        detected_jumps = []

        if self.debug:
            ax = self.plot_flux()

        for i in range(1, len(fluxes)):
            left_flux = np.concatenate(fluxes[:i])
            right_flux = np.concatenate(fluxes[i:])

            if is_all_invalid(left_flux) or is_all_invalid(right_flux):
                continue

            left_last_bin = left_flux[np.isfinite(left_flux)][-1]
            right_first_bin = right_flux[np.isfinite(right_flux)][0]

            left_sigma = deviation_func(left_flux)
            right_sigma = deviation_func(right_flux)
            mean_sigma = (left_sigma + right_sigma) / 2

            if self.debug:
                ax.axvline(times[i][0], c="blue")
                ax.text(
                    times[i][0],
                    1,
                    abs(right_first_bin - left_last_bin) / mean_sigma,
                    rotation=90,
                    fontsize="x-small",
                )

            if abs(right_first_bin - left_last_bin) > detection_threshold * mean_sigma:
                detected_jumps.append(len(left_flux))
                print(
                    f"detected jump at time {times[i][0]}, threshold {abs(right_first_bin - left_last_bin) / mean_sigma}"
                )

        for jump in detected_jumps:
            self.jumps[jump] = {}

            left_flux = self.flux[:jump]
            right_flux = self.flux[jump:]

            if is_all_invalid(left_flux) or is_all_invalid(right_flux):
                continue

            left_last_bin = left_flux[np.isfinite(left_flux)][-1]
            right_first_bin = right_flux[np.isfinite(right_flux)][0]

            left_sigma = deviation_func(left_flux)
            right_sigma = deviation_func(right_flux)
            mean_sigma = (left_sigma + right_sigma) / 2

            if self.debug:
                ax.axvline(self.time[jump], c="red")

            if abs(right_first_bin - left_last_bin) > correction_threshold * mean_sigma:
                self.jumps[len(left_flux)] = {}
                print(
                    f"corrected jump at time {self.time[jump]}, threshold {abs(right_first_bin - left_last_bin) / mean_sigma}"
                )

        self.add_operation(f"corrected {len(self.jumps)} section blocks to equalize their means")
        self.stitch(stitching_mode="total-mean")
        self.jumps = original_jumps
        return self

    def remove_isolated_points(
        self, isolated_of_more_than_days=5 / 24, min_n_points=10, removal_mode="NaN"
    ):
        lc_copy = self.copy()
        lc_copy.jumps = {}
        lc_copy.find_jumps_in_time(min_jump_days=isolated_of_more_than_days)
        time_blocks = lc_copy.divide_flux_between_jumps()[0]
        to_remove = []
        for time_block in time_blocks:
            if len(time_block[np.isfinite(time_block)]) <= min_n_points:
                to_remove += [True] * len(time_block)
            else:
                to_remove += [False] * len(time_block)
        self.remove_data(to_remove, removal_mode)
        return self

    # * -------------------- FILTERING -------------------- * #

    def apply_filter(self, filter_func, window_duration, section_mode, default_padding):
        """
        Applies a given filter to the flux array.
        Input:
        - filter, a function which is given all or a part of the flux vector, and must return a
          vector of equal length in order to have a definite behaviour
        - a window duration, in days, which will be converted to the correspondig time bins size
        - a section mode, either 'section' which applies the filter in between each jump, or 'all'
          which applies the filter to the entirety of the flux
        The filter function will be called with three arguments:
        - a flux array
        - the window size, in time bins
        - a (left, right) tuple corresponding to the padding modes for the flux section
        """
        # if some values are very close to zero, most filtering functions go mad
        self.force_positive()
        window_size = self.duration_to_odd_size(window_duration)

        if section_mode == "all":
            self.flux = filter_func(self.flux, window_size, default_padding)

        elif section_mode == "section":
            times, fluxes, _, padding_modes_list = self.divide_flux_between_jumps(
                default_padding=default_padding
            )
            modified_lightcurves = []
            for l, padding_modes in zip(fluxes, padding_modes_list):
                if not is_all_invalid(l):
                    modified_lightcurves.append(filter_func(l, window_size, padding_modes))
                else:
                    modified_lightcurves.append(l)
            self.flux = np.concatenate(modified_lightcurves)

        else:
            raise ValueError(f"unknown section mode '{section_mode}'")

        self.add_operation(
            f"applied filter {filter_func.__name__}, window duration of {window_duration:.2f} d, "
            + f"window size of {window_size}"
            + f" section-by-section" * (section_mode == "section")
        )
        return self

    def smooth_boxcar_filter(
        self,
        cut_below_period=1,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        return self.apply_filter(
            boxcar_filter,
            cut_below_period / SMOOTH_BOXCAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def smooth_triangular_filter(
        self,
        cut_below_period=1,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        return self.apply_filter(
            triangular_filter,
            cut_below_period / SMOOTH_TRIANGULAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def smooth_sin4_filter(
        self,
        cut_below_period=1,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        return self.apply_filter(
            sin4_filter,
            cut_below_period / SMOOTH_SIN4_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_boxcar_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        if divide:

            def boxcar_normalisation(flux, window_size, padding_modes):
                return flux / boxcar_filter(flux, window_size, padding_modes)

        else:

            def boxcar_normalisation(flux, window_size, padding_modes):
                return flux - boxcar_filter(flux, window_size, padding_modes)

        return self.apply_filter(
            boxcar_normalisation,
            cut_above_period / NORMALIZE_BOXCAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_triangular_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        if divide:

            def triangular_normalisation(flux, window_size, padding_modes):
                return flux / triangular_filter(flux, window_size, padding_modes)

        else:

            def triangular_normalisation(flux, window_size, padding_modes):
                return flux - triangular_filter(flux, window_size, padding_modes)

        return self.apply_filter(
            triangular_normalisation,
            cut_above_period / NORMALIZE_TRIANGULAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_sin4_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        if divide:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux / sin4_filter(flux, window_size, padding_modes)

        else:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux - sin4_filter(flux, window_size, padding_modes)

        return self.apply_filter(
            sin4_normalisation,
            cut_above_period / NORMALIZE_SIN4_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_sin4_3_times_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        # TODO test it with divide
        if divide:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux / sin4_filter(flux, window_size, padding_modes)

        else:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux - sin4_filter(flux, window_size, padding_modes)

        def sin4_normalisation_3_times(flux, window_size, padding_modes):
            sin4_1 = sin4_normalisation(flux, window_size, padding_modes)
            sin4_2 = sin4_normalisation(sin4_1, window_size, padding_modes)
            return sin4_normalisation(sin4_2, window_size, padding_modes)

        return self.apply_filter(
            sin4_normalisation_3_times,
            cut_above_period / NORMALIZE_SIN4_3_TIMES_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_adaptative_filter(
        self,
        cutoff_periods,
        thresholds,
        filter_func,
        period_window_diviser,
        return_indicators,
        default_padding,
    ):
        """
        Applies an adaptative filter which automatically switches from a little window size to a big
        one. See the documentation of `filtering.adaptive_filter` for more theorical information.
        Input:
        - period_cutoffs, a tuple of two durations: (little_filter_cutoff, big_filter_cutoff) for
          the little and big filter used.
        - thresholds, a tuple of two floats: (min_threshold, max_threshold) in between which the
          filter will alternate between big and little filter
        - filter_func, the filter function to be used
        - period_window_diviser, the number by which divide the cutoff period to obtain the window
          duration
        - return_indicators, if True, the function will return an array whose non-zero values
          correspond to where the little filter was used and with which intensity, to be used with
          the `plot_corrections` method
        Outputs either the lightcurve object, or the indicators if return_indicators is True.
        """
        window_duration_1 = self.duration_to_odd_size(cutoff_periods[0] / period_window_diviser)
        window_duration_2 = self.duration_to_odd_size(cutoff_periods[1] / period_window_diviser)
        filtered, indicator_outlier = adaptive_filter(
            self.flux,
            (window_duration_1, window_duration_2),
            thresholds,
            padding_modes=default_padding,
            filter_function=filter_func,
            debug=self.debug,
            time=self.time,
        )
        self.flux = self.flux / filtered
        percentage_little_filter = sum(indicator_outlier > 0) / len(indicator_outlier) * 100

        self.add_operation(
            f"normalized with adaptative filter {filter_func.__name__}, cutoff periods of "
            + f"{cutoff_periods[0]:.2f} d and {cutoff_periods[1]:.2f} d, from threshold "
            + f"{thresholds[0]:.4f} to {thresholds[1]:.4f}, little filter used for "
            + f"{percentage_little_filter:.2f}% of data"
        )

        if return_indicators:
            return indicator_outlier
        else:
            return self

    def normalize_adaptative_boxcar_filter(
        self,
        cut_above_periods=(0.5, 30),
        thresholds=(0.8, 1.3),
        return_indicators=False,
        default_padding=("mirror_both", "mirror_both"),
    ):
        return self.normalize_adaptative_filter(
            cut_above_periods,
            thresholds,
            boxcar_filter,
            NORMALIZE_BOXCAR_FILTER_DIVISER,
            return_indicators,
            default_padding,
        )

    def normalize_adaptative_triangular_filter(
        self,
        cut_above_periods=(0.5, 30),
        thresholds=(0.8, 1.3),
        return_indicators=False,
        default_padding=("mirror_both", "mirror_both"),
    ):
        return self.normalize_adaptative_filter(
            cut_above_periods,
            thresholds,
            triangular_filter,
            NORMALIZE_TRIANGULAR_FILTER_DIVISER,
            return_indicators,
            default_padding,
        )

    def normalize_adaptative_sin4_filter(
        self,
        cut_above_periods=(0.5, 30),
        thresholds=(0.8, 1.3),
        return_indicators=False,
        default_padding=("mirror_both", "mirror_both"),
    ):
        return self.normalize_adaptative_filter(
            cut_above_periods,
            thresholds,
            sin4_filter,
            NORMALIZE_SIN4_FILTER_DIVISER,
            return_indicators,
            default_padding,
        )

    # * -------------------- SECTIONS CORRECTIONS -------------------- * #

    def find_jumps_in_time(self, min_jump_days=3, max_jump_days=np.inf):
        """
        Adds a jump to the lightcurve for each hole in the time array that is bigger than its
        argument.
        """
        lc_copy = self.copy()
        lc_copy.remove_invalids(removal_mode="delete")
        diff = np.diff(lc_copy.time)
        holes = 1 + np.flatnonzero(np.logical_and(diff >= min_jump_days, diff < max_jump_days))
        n_jumps = 0
        for hole_index_without_nan in holes:
            hole_time = lc_copy.time[hole_index_without_nan]
            hole_index = np.searchsorted(self.time, hole_time, side="left")
            if not hole_index in self.jumps:
                self.jumps[hole_index] = {}
                n_jumps += 1
        self.jumps = dict(sorted(self.jumps.items()))
        self.add_operation(f"found {n_jumps} new jumps in time")
        return self

    def find_jumps_in_flux(self, threshold=10, deviation_mode="std"):
        """
        Adds a jump to the lightcurve for each sudden change in flux, by taking the outliers in the
        derivative.
        Argument "threshold" indicate how much outside of the deviation the derivative must be to be
        considered an outlier, and "deviation_mode" is either "std" (by default) or "iqr".
        """
        lc_copy = self.copy()
        lc_copy.remove_invalids(removal_mode="delete")
        deriv = np.diff(lc_copy.flux) / np.diff(lc_copy.time)
        mean = np.nanmean(deriv)
        deviation_func = get_deviation(deviation_mode)
        sigma = deviation_func(deriv)
        jumps = 1 + np.flatnonzero(abs(deriv - mean) > threshold * sigma)
        n_jumps = 0
        for hole_index_without_nan in jumps:
            hole_time = lc_copy.time[hole_index_without_nan]
            hole_index = np.searchsorted(self.time, hole_time, side="left")
            if not hole_index in self.jumps:
                self.jumps[hole_index] = {}
                n_jumps += 1
        self.jumps = dict(sorted(self.jumps.items()))
        self.add_operation(f"found {n_jumps} new jumps in flux")
        return self

    def find_jumps_between_sectors(self):
        """
        Adds a jump to the lightcurve between each sector.
        """
        n_jumps = 0
        sectors, sector_indexes = np.unique(self.sector, return_index=True)
        for sector, sector_index in zip(sectors, sector_indexes):
            if sector_index > 0 and not sector_index in self.jumps:
                self.jumps[sector_index] = {}
                n_jumps += 1

        self.jumps = dict(sorted(self.jumps.items()))
        self.add_operation(f"found {n_jumps} new jumps between sectors")
        return self

    def remove_large_jumps(self, min_jump_days=90, dt="median"):
        # remove invalid data in-between jumps
        lc_copy = self.copy()
        lc_copy.jumps = {}
        lc_copy.remove_invalids(removal_mode="delete")
        lc_copy.find_jumps_in_time(min_jump_days=min_jump_days)

        for jump in list(lc_copy.jumps):
            valid_time_before = lc_copy.time[jump - 1]
            valid_time_after = lc_copy.time[jump]
            time_bins_to_remove = np.logical_and(
                self.time > valid_time_before, self.time < valid_time_after
            )
            self.remove_data(time_bins_to_remove, removal_mode="delete")

        # move around the time array to close the gap
        lc_copy = self.copy()
        lc_copy.jumps = {}
        lc_copy.find_jumps_in_time(min_jump_days=min_jump_days)

        for i_jump in range(len(list(lc_copy.jumps))):
            jump = list(lc_copy.jumps)[i_jump]
            jump_before = 0
            if i_jump > 0:
                jump_before = list(lc_copy.jumps)[i_jump - 1]

            cadence_calc_time = self.time[jump_before:jump]
            if dt == "median":
                dt = np.median(np.diff(cadence_calc_time))
            elif dt == "mean":
                dt = np.mean(np.diff(cadence_calc_time))

            move_by = -self.time[jump] + self.time[jump - 1] + dt
            corresponding_time_jump_index = searchsorted_with_nan_handling(
                self.corresponding_time, self.time[jump]
            )

            self.time[jump:] += move_by
            self.corresponding_time[corresponding_time_jump_index:] += move_by

        return self

    def rescale_sections(self, deviation_mode="iqr"):
        """
        Rescale the sections in order to equalize their deviation for a given deviation mode, which
        can be either "std" for the standard deviation or "iqr" for the interquartile range.
        "iqr" is the default mode, because it permits not to take into account the outliers which
        can be very significant for noisy sections (which we are rescaling for).
        """
        fluxes = self.divide_flux_between_jumps()[1]
        if len(fluxes) <= 1:
            return self

        deviation_func = get_deviation(deviation_mode)

        previous_deviation = np.nan
        for i in range(len(fluxes)):
            if np.isnan(previous_deviation):
                previous_deviation = deviation_func(fluxes[i])
                continue
            fluxes[i] = fluxes[i] / np.nanmean(fluxes[i])
            current_deviation = deviation_func(fluxes[i])
            if np.isfinite(deviation_func(fluxes[i - 1])):
                previous_deviation = deviation_func(fluxes[i - 1])
            fluxes[i] = 1 + (fluxes[i] - 1) * previous_deviation / current_deviation

        self.flux = np.concatenate(fluxes)

        self.add_operation(f"rescaled sections with {deviation_mode} deviation mode")
        return self

    # * -------------------- POINTS CORRECTIONS -------------------- * #

    def remove_data(self, indexes, removal_mode="delete"):
        """
        Removes the given data from the time, flux and quality array.
        Input:
        - an array of indexes to be removed
        - a removal mode, which can be either "delete" to entirely remove the values from time and
          flux arrays, "NaN" to NaN the corresponding flux indexes, or "zero" to zero out the flux.
        """
        if removal_mode == "NaN":
            self.flux[indexes] = np.nan
        elif removal_mode == "zero":
            self.flux[indexes] = 0.0
        elif removal_mode == "delete":
            time_to_remove = self.time[indexes]
            new_time = np.delete(self.time, indexes)
            new_jumps = {}
            for jump_index in self.jumps:
                new_index = np.searchsorted(new_time, self.time[jump_index])
                # do not add jump at the beginning and at the end
                if 0 < new_index < len(new_time) - 1:
                    new_jumps[new_index] = self.jumps[jump_index]

            self.corresponding_time = np.where(
                np.isin(self.corresponding_time, self.time[indexes]),
                np.nan,
                self.corresponding_time,
            )
            self.time = new_time
            self.flux = np.delete(self.flux, indexes)
            self.jumps = dict(sorted(new_jumps.items()))
        else:
            raise ValueError(
                f"could not find right data removal mode, unknown mode '{removal_mode}'"
            )
        return self

    def remove_outliers(
        self,
        identification_mode="sigma_clipping",
        threshold=5,
        removal_mode="NaN",
        deviation_mode="std",
        after_normalisation=None,
    ):
        """
        Removes the outlying flux values from the data, using a clipping algorithm.
        Sigma clipping removes the flux values that lie outside of threshold*deviation of the flux
        (corresponding to an outlier in by comparison of the rest of the flux).
        Derivative sigma clipping removes the values whose derivative lie outside of
        threshold*deviation of the derivative (corresponding to a sudden burst in flux).
        Input:
        - an outliers identification mode, either "sigma_clipping" (by default), or
          "deriv_sigma_clipping"
        - a float threshold, corresponding to the amount of data to remove: as threshold tends
          towards 0, all the data is progressively removed
        - a removal mode of the faulty data, by default "delete"
        - a deviation mode, by default "std"; "iqr" does not produce very good results here
        - a normalisation filter to apply to the flux before removing outliers (which is not applied
          to the final lightcurve), in order to remove long-trend changes before removal
        """
        original_length = len(self.flux)
        deviation_func = get_deviation(deviation_mode)
        if after_normalisation:
            lc_copy = after_normalisation(self.copy())
            assert len(lc_copy.time) == len(
                self.time
            ), "normalisation filter should not change the size of the data"
        else:
            lc_copy = self
        if identification_mode == "sigma_clipping":
            mean = np.nanmean(lc_copy.flux)
            sigma = deviation_func(lc_copy.flux)
            to_remove = abs(lc_copy.flux - mean) > threshold * sigma
            self.remove_data(to_remove, removal_mode=removal_mode)
            n_removed = sum(to_remove)
        elif identification_mode == "deriv_sigma_clipping":
            deriv = np.diff(lc_copy.flux) / np.diff(lc_copy.time)
            mean = np.nanmean(deriv)
            sigma = deviation_func(deriv)
            to_remove = abs(deriv - mean) > threshold * sigma
            self.remove_data(np.concatenate([[False], to_remove]), removal_mode=removal_mode)
            lc_copy.remove_data(np.concatenate([[False], to_remove]), removal_mode=removal_mode)
            n_removed = sum(to_remove)
            while sum(to_remove) > 0:
                deriv = np.diff(lc_copy.flux) / np.diff(lc_copy.time)
                to_remove = abs(deriv - mean) > threshold * sigma
                self.remove_data(np.concatenate([[False], to_remove]), removal_mode=removal_mode)
                lc_copy.remove_data(np.concatenate([[False], to_remove]), removal_mode=removal_mode)
                n_removed += sum(to_remove)
        else:
            raise ValueError(
                f"could not find right outliers removal mode, unknown mode '{identification_mode}'"
            )

        self.add_operation(
            f"removed {n_removed} ({100*n_removed/original_length:.2f}%) outliers with "
            + f"{identification_mode} method, {deviation_mode} deviation and "
            + f"threshold = {threshold}"
        )
        return self

    def resample(self, dt="median"):
        """
        Resamples the data with a new time step. This can mostly be used either to have a constant
        time step (to perform frequency analysis for example), or as a binning method, to both
        reduce the size of the data and remove high-frequency noise.
        If any, the missing data will be sent to NaN.
        Input:
        - the new time step, either "median" (by default) to take the median dt of existing data,
          "mean" to take its mean, or a float
        """
        # dt = 'median'
        t_min = self.time[0]
        t_max = self.time[-1]

        if dt == "median":
            dt = self.dt_median
        elif dt == "mean":
            dt = self.dt_mean

        new_time = np.arange(t_min, t_max + dt / 2, dt)
        N = len(new_time)

        # Initialize new_flux with NaNs to ensure it has the correct length
        new_flux = np.full(N, np.nan)

        new_jumps = {}
        for jump_index in self.jumps:
            new_index = np.searchsorted(new_time, self.time[jump_index])
            new_jumps[new_index] = self.jumps[jump_index]
        self.jumps = dict(sorted(new_jumps.items()))

        # Edges of the time bins
        bin_edges = np.concatenate(([t_min - dt / 2], new_time + dt / 2))

        # Indices of the bins to which each time value belongs
        bin_indices = np.digitize(self.time, bin_edges) - 1

        # Ensure bin indices are within valid range
        bin_indices = np.clip(bin_indices, 0, N - 1)

        bin_indices_original = np.digitize(self.corresponding_time, bin_edges) - 1

        # Sum of flux values for each bin
        keep = np.isfinite(self.flux)
        flux_sum = np.bincount(bin_indices[keep], weights=self.flux[keep], minlength=N)

        # Count of flux values for each bin
        flux_count = np.bincount(bin_indices[keep], minlength=N)

        # Mean flux values for each bin, handling cases where count is zero
        with np.errstate(divide="ignore", invalid="ignore"):
            new_flux = np.true_divide(flux_sum, flux_count)

        self.corresponding_time = np.where(
            np.logical_and(bin_indices_original >= 0, bin_indices_original < N),
            new_time[np.clip(bin_indices_original, 0, N - 1)],
            np.nan,
        )
        self.time = new_time
        self.flux = new_flux

        self.add_operation(f"resampled data to new dt of {dt:.6f} d")
        return self

    def zero_pad(self, N=10, dt="median"):
        """
        Computes zeropading to the lightcurve by concatenating  N zeros arrays to the flux, each with the same lenght as the flux.
        Basicaly your new flux len will be N+1 times the old one's.
        Carefull !! The new time list won't be exactelly regularly sampled !!
        """

        if dt == "median":
            dt = self.dt_median
        elif dt == "mean":
            dt = self.dt_mean

        n = len(self.flux)
        zero_pad = np.zeros(N * n)
        t_pad = np.linspace(self.time[-1] + dt, self.time[-1] + N * n * dt, N * n)

        self.flux = np.concatenate((self.flux, zero_pad))
        self.time = np.concatenate((self.time, t_pad))
        return self

    def flux_to_ppm(self):
        """Centers the flux around 0 and multiply by 10⁶ in order to have PPMs."""
        # centering twice prevent floating point errors to accumulate
        self.flux[self.flux != 0] -= np.nanmean(self.flux[self.flux != 0])
        self.flux *= 1e6
        self.flux[self.flux != 0] -= np.nanmean(self.flux[self.flux != 0])
        self.unit = "ppm"
        self.add_operation(f"centered and multiplied by 10^6 the flux")
        return self

    def force_positive(self):
        """
        Forces the flux to be positive and to have values not too close to 0, in comparison to its
        mean value.
        """
        mean = np.nanmean(self.flux)
        if mean <= 0 or np.nanmin(self.flux) <= abs(mean) / 4:
            self.flux = self.flux + abs(np.nanmin(self.flux)) + abs(mean) / 4
            self.add_operation(f"forced the flux to be positive")
        return self

    def remove_invalids(self, removal_mode="delete"):
        """
        Removes invalid fluxes from the data, which corresponds to NaNs and infinite values.
        Takes as input a removal mode, by default "delete", can sensibly be "zero" to perform
        frequency analysis.
        """
        to_remove = np.logical_not(np.isfinite(self.flux))
        original_len = len(self.flux)
        self.remove_data(to_remove, removal_mode)
        self.add_operation(
            f"removed {sum(to_remove)} invalid data points ({sum(to_remove)/original_len*100:.2f}%)"
        )
        return self

    def remove_quality(self, removal_mode="NaN", quality_to_keep=[-1, 0]):
        """
        Removes flux where the quality flags are not in the list quality_to_keep, by default only 0
        and -1 (meaning no quality data).
        """
        to_remove = ~np.isin(self.quality, quality_to_keep)
        original_len = len(self.flux)
        self.remove_data(to_remove, removal_mode)
        self.add_operation(
            f"removed {sum(to_remove)} bad quality data points ({sum(to_remove)/original_len*100:.2f}%)"
        )
        return self

    # * -------------------- OPERATIONS TRACKING -------------------- * #

    @property
    def debug(self):
        return self._debug

    @debug.setter
    def debug(self, v):
        self._debug = v
        if v:
            self.add_operation("beggining the debug mode")

    def add_operation(self, comment):
        """
        Adds a linear summary of operations that happened to the lightcurve.
        If self.debug is True, it also saves the figure of the flux and psd associated afterwards.
        """
        if self.debug:
            self.operations.append((comment, self.time.copy(), self.flux.copy()))
        else:
            self.operations.append(comment)

    def print_operations(self):
        """
        Prints to stdout the linear summary of operations that have been executed to the data,
        including implicit operations (that have been called by other operations).
        """
        for i, operation in enumerate(self.operations):
            if isinstance(operation, str):
                print(f"{i}: {operation}")
            else:
                comment, time, flux = operation
                print(f"{i}: {comment}")
                fig, (ax1, ax2) = plt.subplots(1, 2, figsize=self.figsize, dpi=self.dpi)
                lc_copy = Lightcurve().init_from_data(time, flux)
                lc_copy.plot_flux(ax=ax1)
                lc_copy.verbose = False
                lc_copy.plot_psd(ax=ax2, correct=True, in_place=True)
                lc_copy.plot_psd(ax=ax2, psd_filter=lambda x: sin4_filter(x, 41))
                fig.suptitle(f"{i}: {comment}", fontsize=12)
                fig.tight_layout()
        return self

    # * -------------------- ANALYSIS -------------------- * #

    def psd(self, zero_padding=0, correct=False, in_place=False, as_hertz=False):
        """
        Calculates the PSD (power spectrum density) of the lightcurve. Equally-spaced data is
        primordial to have correct results, hence the function will warn the user if either the data
        has not been correctly resampled (the time points must be equally spaced), or if the flux is
        not centered around 0 (because the mean value would hinder the PSD).
        Invalid data (NaNs, infs) will be sent to 0 before the PSD analysis.
        Input:
        - a zero padding integer, for each unit, the flux will be padded with equally-as-much of
          zeros to interpolate the PSD, by default 0
        Output:
        - a frequency np.array, typically in 1/days
        - a psd np.array, of the same size
        """
        if in_place:
            lc_cleaned = self
        else:
            lc_cleaned = self.copy()

        time_diff = np.diff(lc_cleaned.time)
        flux_mean = abs(np.nanmean(lc_cleaned.flux))
        max_deviation_from_time_mean = abs(
            np.max(np.diff(lc_cleaned.flux) - np.nanmean(np.diff(lc_cleaned.flux)))
        )
        if abs(np.max(np.diff(lc_cleaned.time) - np.nanmean(np.diff(lc_cleaned.time)))) > 1e-11:
            if self.verbose:
                print("lightcurve was not correctly resampled before the PSD, result will be wrong")
                print(f"maximum deviation from time mean is {max_deviation_from_time_mean} > 1e-11")
            if correct:
                lc_cleaned.resample(self.dt_median)
        if abs(np.nanmean(lc_cleaned.flux)) > 1e-11:
            if self.verbose:
                print("flux mean is not 0 before the PSD, result will be wrong")
                print(f"flux mean is {flux_mean} > 1e-11")
            if correct:
                lc_cleaned.flux_to_ppm()

        lc_cleaned.remove_invalids(removal_mode="zero")
        flux = np.concatenate((lc_cleaned.flux, np.zeros(zero_padding * len(lc_cleaned.flux))))
        freq, psd = apn.psd.series_to_psd(flux, dt=lc_cleaned.dt_median)

        if as_hertz:
            freq = freq / (24 * 3600)
            psd = psd * 24 * 3600

        return freq, psd

    def acf(self):
        """
        Performs autocorrelation for the signal. The flux will be (non-destructively) cleaned with
        invalid data sent to zero, and then centered around 0 in mean.
        Output:
        - lag, a np.array of time values
        - the ACF, a np.array of equal length corresponding to the autocorrelation
        """
        lc_cleaned = self.copy().remove_invalids()
        lc_cleaned.flux[lc_cleaned.flux != 0] -= np.nanmean(lc_cleaned.flux[lc_cleaned.flux != 0])
        dt_median = lc_cleaned.dt_median
        N = len(lc_cleaned.flux)
        lag = np.arange(0, (N + 1 / 10) * dt_median / 2, dt_median)
        corr = np.correlate(lc_cleaned.flux, lc_cleaned.flux, "full")[N - 1 :] / (
            np.var(lc_cleaned.flux) * N
        )
        return lag, corr[: len(lag)]

    # * -------------------- PLOTTING -------------------- * #

    def plot_jumps(self, ax, **kwargs):
        """
        Plots the jumps of the lightcurve on the given axis.
        Additional arguments will be passed to matplotlib.
        Returns the associated axis.
        """
        kwargs.setdefault("c", "black")
        kwargs.setdefault("ls", "--")
        kwargs.setdefault("lw", 0.4)
        kwargs.setdefault("alpha", 0.7)
        kwargs.setdefault("zorder", 1)

        for i, jump_index in enumerate(self.jumps):
            ax.axvline(x=self.time[jump_index], **kwargs)
            if i == 0 and "label" in kwargs:
                ax.legend()
        return ax

    def plot_correction_zones(self, ax, correction, **kwargs):
        """
        Plots the zones which have had corrections in the given axis.
        Input:
        - the matplotlib axis
        - a np.array of the same size of the time array, where the corrections are strictly positive
          values
        Additional arguments will be passed to matplotlib.
        Returns the associated axis.
        """
        kwargs.setdefault("alpha", 0.3)
        ax.fill_between(
            self.time,
            0,
            1,
            where=correction > 0.0,
            transform=ax.get_xaxis_transform(),
            **kwargs,
        )
        if "label" in kwargs:
            ax.legend()
        return ax

    def plot_flux(self, by_sector=False, sector_text=True, sector_text_position=0.05, **kwargs):
        """
        Plots the flux of the lightcurve.
        Input:
        - a boolean for whether or not to ignore nans in the plotting (which would force the visual
          continuity of the flux everywhere)
        Additional arguments will be passed to the `plot` function, and then to matplotlib.
        Returns the associated axis.
        """
        time = self.time
        flux = self.flux

        kwargs.setdefault("title", "Flux")
        kwargs.setdefault("xlabel", r"Time (d)")
        kwargs.setdefault("ylabel", rf"Flux ({self.unit})")
        kwargs.setdefault("s", 3)

        if "markersize" in kwargs:
            kwargs["s"] = kwargs.pop("markersize")

        if by_sector:
            sector_list = self.sector
            unique_sec = np.unique(self.sector)
            ax = kwargs.pop("ax", None)
            for sector in unique_sec:
                sector_mask = sector_list == sector
                ax = scatter(
                    time[sector_mask],
                    flux[sector_mask],
                    ax=ax,
                    figsize=self.figsize,
                    dpi=self.dpi,
                    **kwargs,
                )
                if sector_text:
                    if is_all_invalid(flux[sector_mask]):
                        continue

                    ax.text(
                        time[sector_mask][np.isfinite(flux[sector_mask])][0],
                        sector_text_position,
                        f"{int(sector)}",
                        fontsize="x-small",
                        transform=transforms.blended_transform_factory(ax.transData, ax.transAxes),
                    )
            sector_mask = sector_list == unique_sec[-1]
            return ax
        else:
            return scatter(time, flux, figsize=self.figsize, dpi=self.dpi, **kwargs)

    def plot_flux_top_xticks(self, by_sector=False, **kwargs):
        """
        Plots the flux of the lightcurve.
        Input:
        - a boolean for whether or not to ignore nans in the plotting (which would force the visual
          continuity of the flux everywhere)
        Additional arguments will be passed to the `plot` function, and then to matplotlib.
        Returns the associated axis.
        """
        print("function not working correctly")

        time = self.time
        flux = self.flux

        kwargs.setdefault("title", "Flux")
        kwargs.setdefault("xlabel", r"Time (d)")
        kwargs.setdefault("ylabel", rf"Flux ({self.unit})")
        kwargs.setdefault("s", 3)

        if "markersize" in kwargs:
            kwargs["s"] = kwargs.pop("markersize")

        if by_sector:
            sector_list = self.sector
            unique_sec = np.unique(self.sector)
            for sector in unique_sec:
                sector_mask = sector_list == sector
                ax = scatter(
                    time[sector_mask],
                    flux[sector_mask],
                    figsize=self.figsize,
                    dpi=self.dpi,
                    **kwargs,
                )
                ax.text(
                    time[sector_mask][0],
                    np.nanmean(flux) + np.nanstd(flux),
                    f"S{int(sector)}",
                    fontsize="x-small",
                )
            sector_mask = sector_list == unique_sec[-1]
        else:
            ax = scatter(time, flux, figsize=self.figsize, dpi=self.dpi, **kwargs)

        ticks_location = ax.get_xticks()
        ax_top = ax.twiny()
        ax_top.set_xlim(ax.get_xlim())
        ax_top.set_xticks(ticks_location)

        time_to_original_indexes = np.clip(
            searchsorted_with_nan_handling(self.corresponding_time, ticks_location),
            0,
            len(self.original_time) - 1,
        )
        labels = self.original_time[time_to_original_indexes].astype(int)
        ax_top.set_xticklabels(labels)
        return ax

    def plot_flux_nans(self, **kwargs):
        """
        Plots the NaNs of the flux, in red by default.
        """
        to_plot = np.logical_not(np.isfinite(self.flux))

        kwargs.setdefault("title", "Flux")
        kwargs.setdefault("xlabel", r"Time (d)")
        kwargs.setdefault("ylabel", rf"Flux ({self.unit})")
        kwargs.setdefault("s", 1)
        kwargs.setdefault("c", "red")
        kwargs.setdefault("alpha", 0.3)
        return scatter(
            self.time[to_plot],
            to_plot[to_plot],
            figsize=self.figsize,
            dpi=self.dpi,
            **kwargs,
        )

    def plot_psd(
        self,
        visualisation="Hz",
        zero_padding=0,
        psd_filter=lambda x: x,
        correct=False,
        in_place=False,
        nu_max=None,
        **kwargs,
    ):
        """
        Plots the PSD of the lightcurve.
        Input:
        - a visualisation mode for the x-axis, either "Hz" to have μHz, "1/d" to have d⁻¹, or "d" to
          plot in periodicity rather than frequency and have days
        - the number of zero padding to include in the PSD calculation
        - whether or not to correct the data before the PSD
        - an optionnal psd_filter which is applied to the PSD before plotting it, useful to obtain
          readable values without a lot of scientific sense
        Additional arguments will be passed to the `plot` function, and then to matplotlib.
        Returns the associated axis.
        """
        freq, psd = self.psd(zero_padding, correct, in_place)

        if visualisation == "1/d":
            freq = freq[1:]
            psd = psd[1:]
            kwargs.setdefault("xlabel", r"Frequency (d$^{-1}$)")
            kwargs.setdefault("ylabel", rf"PSD ({self.unit}$^2 \cdot$ d)")
        elif visualisation == "Hz":
            freq = freq[1:] * 1e6 / (24 * 3600)
            psd = psd[1:] * 1e-6 * 24 * 3600
            kwargs.setdefault("xlabel", r"Frequency ($\mu$Hz)")
            kwargs.setdefault("ylabel", rf"PSD ({self.unit}$^2$ / $\mu$Hz)")
        elif visualisation == "d":
            freq = 1 / freq[1:]
            psd = psd[1:]
            kwargs.setdefault("xlabel", r"Period (d)")
            kwargs.setdefault("ylabel", rf"PSD ({self.unit}$^2$ / d)")
        else:
            raise ValueError(f"could not find the visualisation mode '{visualisation}'")

        kwargs.setdefault("title", "PSD")
        kwargs.setdefault("xscale", "log")
        kwargs.setdefault("yscale", "log")
        kwargs.setdefault("lw", 0.8)
        ax = plot(freq, psd_filter(psd), figsize=self.figsize, dpi=self.dpi, **kwargs)

        if nu_max is not None:
            ax.axvline(x=nu_max, c="red", ls="--", lw=1.5, alpha=0.8, zorder=1)

        return ax

    def plot_acf(self, **kwargs):
        """
        Plots the ACF of the lightcurve.
        Any argument will be passed to the `plot` function, and then to matplotlib.
        Returns the associated axis.
        """
        lag, acf = self.acf()
        kwargs.setdefault("title", "ACF")
        kwargs.setdefault("xlabel", r"Lag (d)")
        kwargs.setdefault("ylabel", r"ACF")
        kwargs.setdefault("alpha", 1)
        kwargs.setdefault("lw", 0.7)
        return plot(lag, acf, figsize=self.figsize, dpi=self.dpi, **kwargs)
