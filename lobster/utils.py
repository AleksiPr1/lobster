import numpy as np
from astropy.io import fits
import datetime


def searchsorted_with_nan_handling(arr, values):
    # Create a mask for NaN values
    nan_mask = np.isnan(arr)
    # Indices and values for non-NaN elements
    non_nan_indices = np.where(~nan_mask)[0]
    non_nan_values = arr[non_nan_indices]

    # Handle scalar and array inputs separately
    if np.isscalar(values):
        # Perform searchsorted on the non-NaN values
        index = np.searchsorted(non_nan_values, values)
        # Count NaNs before the scalar value
        nan_count_before = np.sum(nan_mask & (arr < values))
        # Adjust the index
        adjusted_index = non_nan_indices[index] + nan_count_before
        return adjusted_index
    else:
        # Perform searchsorted on the non-NaN values for array inputs
        indices = np.searchsorted(non_nan_values, values)
        # Expand dimensions for broadcasting
        values_expanded = values[:, np.newaxis]
        # Compute how many NaNs are before each value in values
        nan_counts = np.sum(nan_mask & (arr < values_expanded), axis=1)
        # Adjust the indices
        adjusted_indices = non_nan_indices[indices] + nan_counts
        return adjusted_indices


def get_deviation(deviation_mode):
    """
    Returns the function doing the calculation of the given deviation model, either "std" for
    standard deviation or "iqr" for interquartile range.
    """
    if deviation_mode == "iqr":
        return lambda x: np.subtract(*np.percentile(x, [75, 25]))
    elif deviation_mode == "std":
        return np.nanstd
    else:
        raise ValueError(f"could not find appropriate deviation mode for '{deviation_mode}'")


def is_all_invalid(array):
    """Returns True if and only if every value in the array is either NaN or infinite."""
    return np.all(np.logical_not(np.isfinite(array)))


def write_to_fits(time, flux, operations, unit, fits_path, object_name=None):
    """
    Permits to write the time and flux array onto a FITS file and save it.
    If given object_name, then it will be written in the Primary HDU header.
    This function is heavily based on the equivalent Lightkurve method.
    """
    # Primary HDU, contains general data about the lightcurve
    primary_hdu = fits.PrimaryHDU()

    default = {
        "DATE": datetime.datetime.now().strftime("%Y-%m-%d"),
        "CREATOR": "Lobster",
    }

    if object_name:
        default["OBJECT"] = object_name

    for kw in default:
        primary_hdu.header[kw] = default[kw]

    # Lightcurve HDU, contains the TIME and FLUX values
    cols = [
        fits.Column(
            name="TIME",
            format="D",
            unit="d",
            array=time,
        ),
        fits.Column(
            name="FLUX",
            format="D",
            unit=unit,
            array=flux,
        ),
    ]

    coldefs = fits.ColDefs(cols)
    lightcurve_hdu = fits.BinTableHDU.from_columns(coldefs)
    lightcurve_hdu.header["EXTNAME"] = "LIGHTCURVE"

    for i, operation in enumerate(operations):
        if isinstance(operation, tuple):
            operation = operation[0]
        lightcurve_hdu.header["HISTORY"] = f"{i}: {operation}"

    # write both into the FITS file
    fits.HDUList(
        [
            primary_hdu,
            lightcurve_hdu,
        ]
    ).writeto(fits_path, overwrite=True)
